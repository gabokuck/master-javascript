'use strict'

// Pruebas con let y var
var numero = 40;
console.log(numero) // valor 40

// Prueva con var
if(true){
	var numero = 50;
	console.log(numero) // valor 50
}

console.log(numero); // valor 50

// Prueba con let
var texto = "Curso js"
console.log(texto) // muestra ""

if(true){
	let texto = "Curso tomado por Gabriel"
	console.log(texto); // valor curso tomado por Gabriel
}

console.log(texto) //  muestra Curso js