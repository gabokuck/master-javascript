'use strict'

/*
1. Pida 6 números por pantalla y los meta por un array
2. Mostrar el array entero / (todos sus elementos) en el cuerpo de la pagina y en la consola
3. Ordenarlo y mostrarlo
4. Invertir su orden y mostrarlo
5. Mostrar cuantos elementos tiene el array
6. Busqueda de un valor introducido por el usuario, que nos diga si lo encuentra y su indice
*/

function mostrarArray(elementos, textoCustom = "") {
    document.write("<h1>Contenido del array " + textoCustom + "</h1>");
    document.write("<ul>");
    elementos.forEach((elemento, index) => {
        document.write("<li>" + elemento + "</li>")
    });
    document.write("</ul>");
}

//var numeros = new Array(6);

// Pedir 6 números
var numeros = [];

for (var i = 0; i <= 5; i++) {
    //numeros[i] = parseInt(prompt("Introduce un número", 0));
    numeros.push(parseInt(prompt("Introduce un número", 0)));
}

//Mostar en el cuerpo de la página
mostrarArray(numeros);


// Mostrar Array por la consola
console.log(numeros);

//Ordenar y mostrar
numeros.sort(function(a, b) { return a - b });
mostrarArray(numeros, 'Ordenados');

//Invertir y mostrar
numeros.reverse();
mostrarArray(numeros, 'revertido');

// Contar elementos
document.write("<h1>El array tiene:  " + numeros.length + "elementos</h1>");

// Busqueda 

var busqueda = parseInt(prompt("Busca un número", 0));

var posicion = numeros.findIndex(numero => numero == busqueda);

if (posicion && posicion != -1) {
    document.write("<hr><h1>Encontrado</h1>");
    document.write("<h1>Posición de la busqueda: " + posicion + "</h1><hr>");
} else {
    document.write("<hr><h1>No encontrado!!!</h1>")
}