'use strict'

// Localstorage

// Comprobar disponibilidad del LocalStorage
if (typeof(Storage) !== 'undefined') {
    console.log('LocalStorage Disponible');
} else {
    console.log('Incompatible con LocalStorage');
};

// Guardar datos
localStorage.setItem("titulo", "Curso de Symfony");

// Recuperar elemento
document.querySelector("#peliculas").innerHTML = localStorage.getItem("titulo");

// Guardad objetos
var usuario = {
    nombre: 'Gabriel Rosa',
    email: 'gabo_kuck@gmail.com',
    web: 'gabo.com'
};

localStorage.setItem("usuario", JSON.stringify(usuario));

// Recuperar objeto
var userjs = JSON.parse(localStorage.getItem("usuario"));
console.log(userjs);
document.querySelector("#datos").append(" " + userjs.web + " - " + userjs.nombre);

localStorage.removeItem("usuario");

localStorage.clear();