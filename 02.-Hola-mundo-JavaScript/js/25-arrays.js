'use strict'

// Arrays, Arreglos, Matrices

var nombre = "Gabriel Rosa";
var nombres = ["Gabriel Rosa", "Alfonso Rosa", "Blanca Gabriela Corona Rosas", "Hugo Corona", 52, true];

var lenguajes = new Array("php", "js", "go", "java", "c#", "c", "pascal");

/*
var elemento = parseInt(prompt("Que elemnto del array quieres?", 0));
if (elemento >= nombres.length) {
    alert("Introduce el número correcto menor que " + nombres.length)
} else {
    alert("El usuario seleccionado es: " + nombres[elemento]);
}
*/

document.write("<h1>Lenguajes de programación del 2018</h1>");
document.write("<ul>");
/*
for (var i = 0; i < lenguajes.length; i++) {
    document.write("<li>" + lenguajes[i] + "</li>");
}
*/

lenguajes.forEach((elemento, indice, arr) => {
    console.log(arr);
    document.write("<li>" + indice + " - " + elemento + "</li>");
});

document.write("</ul>");