'use strict'

/*
Calculadora:
-Pida dos números por pantalla
-Si metemos uno mal que nos los vuelva a pedir
-En el cuerpo de la pagina en un alert y por consola el resultado de
sumar, restar, mutiplicar, dividir esas dos cifras.
*/

var numero1 = parseInt(prompt("Introduce el primer número", 0));
var numero2 = parseInt(prompt("Introduce el segundo número", 0));

while(numero1 < 0 || numero2 < 0 || isNaN(numero1) || isNaN(numero2)){
	numero1 = parseInt(prompt("Introduce el primer número", 0));
	numero2 = parseInt(prompt("Introduce el segundo número", 0));
}

var resultado = "La suma es: " + (numero1 + numero2) + "<br>" +
				"La resta es: " + (numero1 - numero2) + "<br>" +
				"La Multiplicación es: " + (numero1 * numero2) + "<br>" +
				"La División es: " + (numero1 / numero2) + "<br>";

var resultado2 = "La suma es: " + (numero1 + numero2) + "\n" +
				"La resta es: " + (numero1 - numero2) + "\n" +
				"La Multiplicación es: " + (numero1 * numero2) + "\n" +
				"La División es: " + (numero1 / numero2) + "\n";

document.write(resultado);
alert(resultado2)
console.log(resultado2)